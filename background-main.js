/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
function onContextMenuLookupClick(info,tab) {
    const text = info.selectionText.trim().toLowerCase();
    const createTabProps = {
        active: true,
        openerTabId: tab.id,
        url: 'https://www.wordnik.com/words/' + encodeURIComponent(text)
    };
    browser.tabs.create(createTabProps);
}

function main() {

    const createMenuLookupProps = {
        id: "context-commands-lookup",
        onclick: onContextMenuLookupClick,
        title: "lookup '%s'",
        contexts: ["selection"]
    };

    browser.menus.create(createMenuLookupProps);
}
main();