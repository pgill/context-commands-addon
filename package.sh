#
# This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
# 1991.
#
#!/bin/env bash

# switch to root directory
SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")
cd "$DIR"

PACKAGE_NAME="context-commands-addon.zip"

if [ -f $PACKAGE_NAME ]; then
    echo "deleting $PACKAGE_NAME"
    rm $PACKAGE_NAME
fi

echo "packaging to $PACKAGE_NAME"
zip -r -FS $PACKAGE_NAME * -x $PACKAGE_NAME package.sh